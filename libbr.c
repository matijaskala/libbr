#include <stdlib.h>
#include <string.h>

#include <brotli/decode.h>
#include <brotli/encode.h>
#include <sha256.h>
#include <xxhash.h>

#include "crc32c/gsb_crc32.h"
#include "libbr.h"

struct _br_stream
{
    unsigned const char *nextIn;
    unsigned char *nextOut;
    unsigned long int availIn;
    unsigned long int availOut;
    XXH32_state_t *xxh32;
    XXH64_state_t *xxh64;
    XXH32_hash_t xxh32sum;
    XXH64_hash_t xxh64sum;
    XXH32_state_t *xxh32Full;
    XXH64_state_t *xxh64Full;
    uint32_t crc32csum;
    uint32_t crc32cFull;
    SHA256_CTX sha256;
    BrotliDecoderState *decoder;
    BrotliEncoderState *encoder;
    uint64_t totalSize;
    uint64_t blockSize;
    uint64_t maxBlockSize;
    uint64_t offset;
    uint64_t oldOffset;
    uint8_t contentMask;
    uint8_t extraMask;
    int8_t checkType;
    int8_t level;
    uint64_t tmp1;
    uint64_t tmp2;
    uint8_t tmp3[32];
    int continueAt;
};

br_stream* br_init()
{
    br_stream* stream = malloc(sizeof *stream);
    stream->level = 9;
    stream->maxBlockSize = 1 << 22;
    stream->availIn = stream->availOut = 0;
    stream->nextIn = stream->nextOut = NULL;
    stream->decoder = NULL;
    stream->encoder = NULL;
    stream->xxh32 = XXH32_createState();
    stream->xxh64 = XXH64_createState();
    stream->xxh32Full = XXH32_createState();
    stream->xxh64Full = XXH64_createState();
    if (!stream->xxh32 || !stream->xxh64 || !stream->xxh32Full || !stream->xxh64Full) {
        br_fini(stream);
        return NULL;
    }
    return stream;
}

void br_fini(br_stream* stream)
{
    if (stream->decoder)
        BrotliDecoderDestroyInstance(stream->decoder);
    if (stream->encoder)
        BrotliEncoderDestroyInstance(stream->encoder);
    if (stream->xxh32)
        XXH32_freeState(stream->xxh32);
    if (stream->xxh64)
        XXH64_freeState(stream->xxh64);
    if (stream->xxh32Full)
        XXH32_freeState(stream->xxh32Full);
    if (stream->xxh64Full)
        XXH64_freeState(stream->xxh64Full);
    free(stream);
}

static int br_reset_internal(br_stream* stream)
{
    if (stream->decoder)
        BrotliDecoderDestroyInstance(stream->decoder);
    if (stream->encoder)
        BrotliEncoderDestroyInstance(stream->encoder);
    stream->decoder = NULL;
    stream->encoder = NULL;
    stream->totalSize = 0;
    stream->offset = 0;
    XXH32_reset(stream->xxh32Full, 0);
    XXH64_reset(stream->xxh64Full, 0);
    stream->crc32cFull = ~calculate_crc32c(~0, NULL, 0);
    stream->continueAt = 0;
    return BR_OK;
}

void br_set_compression_level(br_stream* stream, int level)
{
    if (level < BROTLI_MIN_QUALITY)
        level = BROTLI_MIN_QUALITY;
    if (level > BROTLI_MAX_QUALITY)
        level = BROTLI_MAX_QUALITY;
    stream->level = level;
    if (stream->encoder)
        BrotliEncoderSetParameter(stream->encoder, BROTLI_PARAM_QUALITY, stream->level);
}

void br_set_out_buffer(br_stream* stream, char *data, unsigned int maxlen)
{
    stream->availOut = maxlen;
    stream->nextOut = (unsigned char *)data;
}

void br_set_in_buffer(br_stream* stream, const char *data, unsigned int size)
{
    stream->availIn = size;
    stream->nextIn = (const unsigned char *)data;
}

int br_in_buffer_available(const br_stream* stream)
{
    return stream->availIn;
}

int br_out_buffer_available(const br_stream* stream)
{
    return stream->availOut;
}

br_result br_read_header(br_stream* stream)
{
    if (br_reset_internal(stream) != BR_OK)
        return BR_ERROR;
    if (stream->availIn < 4)
        return BR_ERROR;
    if (stream->nextIn[0] != 0xce || stream->nextIn[1] != 0xb2 || stream->nextIn[2] != 0xcf || stream->nextIn[3] != 0x81)
        return BR_ERROR;
    stream->nextIn += 4;
    stream->availIn -= 4;
    return BR_OK;
}

br_result br_write_header(br_stream* stream, const char* filename)
{
    size_t filename_length = filename ? strlen(filename) : 0;
    size_t header_length = 6;
    if (filename) {
        for (size_t i = filename_length; i > 0x7f; i <<= 7)
            header_length++;
        header_length += 1 + filename_length;
    }
    if (header_length > stream->availOut)
        return BR_ERROR;
    if (br_reset_internal(stream) != BR_OK)
        return BR_ERROR;
    stream->nextOut[0] = 0xce;
    stream->nextOut[1] = 0xb2;
    stream->nextOut[2] = 0xcf;
    stream->nextOut[3] = 0x81;
    stream->nextOut[4] = 0113;
    stream->nextOut[5] = filename ? 2 : 0;
    stream->nextOut += 6;
    stream->availOut -= 6;
    if (filename) {
        size_t i;
        for (i = filename_length; i > 0x7f; i <<= 7) {
            *stream->nextOut++ = i & 0x7f;
            stream->availOut--;
        }
        *stream->nextOut++ = i | 0x80;
        stream->availOut--;
        while (*filename) {
            *stream->nextOut++ = *filename++;
            stream->availOut--;
        }
    }
    return BR_OK;
}

br_result br_uncompress(br_stream* stream)
{
    uint8_t *tmpPtr;
    switch (stream->continueAt) {
        case 0:
            goto cont0;
        case 1:
            goto cont1;
        case 2:
            goto cont2;
        case 3:
            goto cont3;
        case 4:
            goto cont4;
        case 5:
            goto cont5;
        case 6:
            goto cont6;
        case 7:
            goto cont7;
        case 8:
            goto cont8;
        case 9:
            goto cont9;
        case 10:
            goto cont10;
        case 11:
            goto cont11;
        case 12:
            goto cont12;
        case 13:
            goto cont13;
        case 14:
            goto cont14;
        case 15:
            goto cont15;
        case 16:
            goto cont16;
        case 17:
            goto cont17;
        case 18:
            goto cont18;
        case 19:
            goto cont19;
    }
    cont0:
    stream->continueAt = 0;
    if (stream->availIn == 0)
        return BR_OK;
    stream->crc32csum = ~calculate_crc32c(~0, NULL, 0);
    stream->blockSize = 0;
    stream->oldOffset = stream->offset;
    stream->offset = 0;
    if (0x34cb00 >> ((*stream->nextIn ^ (*stream->nextIn >> 4)) & 0xf) & 0x80) {
        return BR_ERROR;
    }
    stream->contentMask = *stream->nextIn & 0x7f;
    if ((stream->contentMask & 0140) == 0140) {
        return BR_ERROR;
    }
    stream->offset++;
    stream->nextIn++;
    stream->availIn--;
    stream->checkType = stream->contentMask & 7;
    if (stream->contentMask & 020) {
        stream->tmp1 = stream->oldOffset;
        if (stream->contentMask & 040) {
            stream->continueAt = 1;
            cont1:
            if (stream->availIn == 0)
                return BR_OK;
            if ((*stream->nextIn & 0x80) == 0) {
                return BR_ERROR;
            }
            if ((*stream->nextIn & 0x7f) != (stream->tmp1 & 0x7f)) {
                return BR_ERROR;
            }
            stream->tmp1 >>= 7;
            stream->offset++;
            stream->nextIn++;
            stream->availIn--;
        }
        do {
            stream->continueAt = 2;
            cont2:
            if (stream->availIn == 0)
                return BR_OK;
            if ((*stream->nextIn & 0x7f) != (stream->tmp1 & 0x7f)) {
                return BR_ERROR;
            }
            stream->tmp1 >>= 7;
            stream->offset++;
            stream->nextIn++;
            stream->availIn--;
        } while ((*(stream->nextIn - 1) & 0x80) == 0);
        if (stream->tmp1 != 0) {
            return BR_ERROR;
        }
    }
    switch (stream->checkType) {
        case 0:
        case 1:
        case 2:
            XXH32_reset(stream->xxh32, 0);
            break;
        case 3:
            XXH64_reset(stream->xxh64, 0);
            break;
        case 4:
        case 5:
        case 6:
            stream->crc32cFull = ~calculate_crc32c(~0, NULL, 0);
            break;
        case 7:
            if (!(stream->contentMask & 040)) {
                stream->continueAt = 3;
                cont3:
                if (stream->availIn == 0)
                    return BR_OK;
                if (stream->contentMask & 040)
                    break;
                if (*stream->nextIn != 0) {
                    return BR_ERROR;
                }
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
                SHA256_Init(&stream->sha256);
            }
    }
    if (stream->contentMask & 0100) {
        stream->continueAt = 4;
        cont4:
        if (stream->availIn == 0)
            return BR_OK;
        if (0x34cb00 >> ((*stream->nextIn ^ (*stream->nextIn >> 4)) & 0xf) & 0x80) {
            return BR_ERROR;
        }
        stream->extraMask = *stream->nextIn;
        stream->offset++;
        stream->nextIn++;
        stream->availIn--;
        if (stream->extraMask & 1)
            do {
                stream->continueAt = 5;
                cont5:
                if (stream->availIn == 0)
                    return BR_OK;
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
            } while ((*(stream->nextIn - 1) & 0x80) == 0);
        if (stream->extraMask & 2) {
            stream->tmp1 = 0;
            stream->tmp2 = 0;
            do {
                if (stream->tmp2 >= 4) {
                    return BR_ERROR;
                }
                stream->continueAt = 6;
                cont6:
                if (stream->availIn == 0)
                    return BR_OK;
                stream->tmp1 += (uint32_t) (*stream->nextIn & 0x7f) << (stream->tmp2++ * 7);
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
            } while ((*(stream->nextIn - 1) & 0x80) == 0);
            stream->offset += stream->tmp1;
            cont7:
            if (stream->tmp1 >= stream->availIn) {
                stream->tmp1 -= stream->availIn;
                stream->availIn = 0;
                stream->continueAt = 7;
                return BR_OK;
            }
            stream->nextIn += stream->tmp1;
            stream->availIn -= stream->tmp1;
        }
        if (stream->extraMask & 4) {
            stream->tmp1 = 0;
            stream->tmp2 = 0;
            do {
                if (stream->tmp2 >= 10) {
                    return BR_ERROR;
                }
                stream->continueAt = 8;
                cont8:
                if (stream->availIn == 0)
                    return BR_OK;
                stream->tmp1 += (uint32_t) (*stream->nextIn & 0x7f) << (stream->tmp2++ * 7);
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
            } while ((*(stream->nextIn - 1) & 0x80) == 0);
            stream->offset += stream->tmp1;
            cont9:
            if (stream->tmp1 >= stream->availIn) {
                stream->tmp1 -= stream->availIn;
                stream->availIn = 0;
                stream->continueAt = 9;
                return BR_OK;
            }
            stream->nextIn += stream->tmp1;
            stream->availIn -= stream->tmp1;
        }
        if (stream->extraMask & 0100) {
            stream->continueAt = 10;
            cont10:
            if (stream->availIn == 0)
                return BR_OK;
            if (*stream->nextIn & 7) {
                return BR_ERROR;
            }
            stream->offset++;
            stream->nextIn++;
            stream->availIn--;
        }
        if (stream->extraMask & 040) {
            stream->tmp1 = 2;
            stream->offset += stream->tmp1;
            cont11:
            if (stream->tmp1 >= stream->availIn) {
                stream->tmp1 -= stream->availIn;
                stream->availIn = 0;
                stream->continueAt = 11;
                return BR_OK;
            }
            stream->nextIn += stream->tmp1;
            stream->availIn -= stream->tmp1;
        }
    }
    stream->decoder = BrotliDecoderCreateInstance(NULL, NULL, NULL);
    if (!stream->decoder) {
        return BR_ERROR;
    }
    if ((stream->contentMask & 040) == 0) {
        stream->continueAt = 12;
        cont12:
        stream->offset += stream->availIn;
        stream->tmp1 = stream->availOut;
        tmpPtr = stream->nextOut;
        switch (BrotliDecoderDecompressStream(stream->decoder, &stream->availIn, &stream->nextIn, &stream->availOut, &stream->nextOut, NULL)) {
            case BROTLI_DECODER_RESULT_SUCCESS:
                stream->offset -= stream->availIn;
                stream->tmp1 -= stream->availOut;
                switch (stream->checkType) {
                    case 0:
                    case 1:
                    case 2:
                        XXH32_update(stream->xxh32, tmpPtr, stream->tmp1);
                        break;
                    case 3:
                        XXH64_update(stream->xxh64, tmpPtr, stream->tmp1);
                        break;
                    case 4:
                    case 5:
                    case 6:
                        stream->crc32csum = ~calculate_crc32c(~stream->crc32csum, tmpPtr, stream->tmp1);
                        break;
                    case 7:
                        SHA256_Update(&stream->sha256, tmpPtr, stream->tmp1);
                }
                stream->totalSize += stream->blockSize += stream->tmp1;
                break;
            case BROTLI_DECODER_RESULT_ERROR:
                return BR_ERROR;
            case BROTLI_DECODER_RESULT_NEEDS_MORE_INPUT:
            case BROTLI_DECODER_RESULT_NEEDS_MORE_OUTPUT:
                stream->offset -= stream->availIn;
                stream->tmp1 -= stream->availOut;
                switch (stream->checkType) {
                    case 0:
                    case 1:
                    case 2:
                        XXH32_update(stream->xxh32, tmpPtr, stream->tmp1);
                        break;
                    case 3:
                        XXH64_update(stream->xxh64, tmpPtr, stream->tmp1);
                        break;
                    case 4:
                    case 5:
                    case 6:
                        stream->crc32csum = ~calculate_crc32c(~stream->crc32csum, tmpPtr, stream->tmp1);
                        break;
                    case 7:
                        SHA256_Update(&stream->sha256, tmpPtr, stream->tmp1);
                }
                stream->blockSize += stream->tmp1;
                return BR_OK;
        }
        BrotliDecoderDestroyInstance(stream->decoder);
        stream->decoder = NULL;
    }
    if (stream->contentMask & 010) {
        stream->tmp1 = (stream->contentMask & 040) ? stream->totalSize : stream->blockSize;
        if (stream->contentMask & 040) {
            stream->continueAt = 13;
            cont13:
            if (stream->availIn == 0)
                return BR_OK;
            if ((*stream->nextIn & 0x80) == 0) {
                return BR_ERROR;
            }
            if ((*stream->nextIn & 0x7f) != (stream->tmp1 & 0x7f)) {
                return BR_ERROR;
            }
            stream->tmp1 >>= 7;
            stream->offset++;
            stream->nextIn++;
            stream->availIn--;
        }
        do {
            stream->continueAt = 14;
            cont14:
            if (stream->availIn == 0)
                return BR_OK;
            if ((*stream->nextIn & 0x7f) != (stream->tmp1 & 0x7f)) {
                return BR_ERROR;
            }
            stream->tmp1 >>= 7;
            stream->offset++;
            stream->nextIn++;
            stream->availIn--;
        } while ((*(stream->nextIn - 1) & 0x80) == 0);
        if (stream->tmp1 != 0) {
            return BR_ERROR;
        }
    }
    switch (stream->checkType) {
        case 0:
        case 1:
        case 2:
            stream->xxh32sum = XXH32_digest(stream->xxh32);
            for (stream->tmp1 = 0; stream->tmp1 < 1u << stream->checkType; stream->tmp1++) {
                stream->continueAt = 15;
                cont15:
                if (stream->availIn == 0)
                    return BR_OK;
                stream->tmp3[stream->tmp1] = stream->xxh32sum & 0xff;
                if (*stream->nextIn != stream->tmp3[stream->tmp1]) {
                    return BR_ERROR;
                }
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
                stream->xxh32sum >>= 8;
            }
            XXH32_update(stream->xxh32Full, stream->tmp3, 1 << stream->checkType);
            XXH64_update(stream->xxh64Full, stream->tmp3, 1 << stream->checkType);
            stream->crc32cFull = ~calculate_crc32c(~stream->crc32cFull, stream->tmp3, 1 << stream->checkType);
            break;
        case 3:
            stream->xxh64sum = XXH64_digest(stream->xxh64);
            for (stream->tmp1 = 0; stream->tmp1 < 1u << stream->checkType; stream->tmp1++) {
                stream->continueAt = 16;
                cont16:
                if (stream->availIn == 0)
                    return BR_OK;
                stream->tmp3[stream->tmp1] = stream->xxh64sum & 0xff;
                if (*stream->nextIn != stream->tmp3[stream->tmp1]) {
                    return BR_ERROR;
                }
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
                stream->xxh64sum >>= 8;
            }
            XXH32_update(stream->xxh32Full, stream->tmp3, 1 << stream->checkType);
            XXH64_update(stream->xxh64Full, stream->tmp3, 1 << stream->checkType);
            stream->crc32cFull = ~calculate_crc32c(~stream->crc32cFull, stream->tmp3, 1 << stream->checkType);
            break;
        case 4:
        case 5:
        case 6:
            for (stream->tmp1 = 0; stream->tmp1 < 1u << (stream->checkType - 4); stream->tmp1++) {
                stream->continueAt = 17;
                cont17:
                if (stream->availIn == 0)
                    return BR_OK;
                stream->tmp3[stream->tmp1] = stream->xxh32sum & 0xff;
                if (*stream->nextIn != stream->tmp3[stream->tmp1]) {
                    return BR_ERROR;
                }
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
                stream->crc32csum >>= 8;
            }
            XXH32_update(stream->xxh32Full, stream->tmp3, 1 << (stream->checkType - 4));
            XXH64_update(stream->xxh64Full, stream->tmp3, 1 << (stream->checkType - 4));
            stream->crc32cFull = ~calculate_crc32c(~stream->crc32cFull, stream->tmp3, 1 << (stream->checkType - 4));
            break;
        case 7:
            if (stream->contentMask & 040)
                break;
            SHA256_Final(stream->tmp3, &stream->sha256);
            for (stream->tmp1 = 0; stream->tmp1 < 32; stream->tmp1++) {
                stream->continueAt = 18;
                cont18:
                if (stream->availIn == 0)
                    return BR_OK;
                if (*stream->nextIn != stream->tmp3[stream->tmp1]) {
                    return BR_ERROR;
                }
                stream->offset++;
                stream->nextIn++;
                stream->availIn--;
            }
            XXH32_update(stream->xxh32Full, stream->tmp3, 32);
            XXH64_update(stream->xxh64Full, stream->tmp3, 32);
            stream->crc32cFull = ~calculate_crc32c(~stream->crc32cFull, stream->tmp3, 32);
    }
    if (stream->contentMask & 040) {
        if (stream->contentMask != 047) {
            stream->continueAt = 19;
            cont19:
            if (stream->availIn == 0)
                return BR_OK;
            if (0x34cb00 >> ((*stream->nextIn ^ (*stream->nextIn >> 4)) & 0xf) & 0x80) {
                return BR_ERROR;
            }
            if ((*stream->nextIn & 0x7f) != stream->contentMask) {
                return BR_ERROR;
            }
            stream->offset++;
            stream->nextIn++;
            stream->availIn--;
        }
        for (uint64_t i = 0; i < stream->availIn; i++)
            if (stream->nextIn[i]) {
                return BR_ERROR;
            }
        stream->continueAt = 0;
        return BR_END;
    }
    goto cont0;
}

br_result br_compress(br_stream* stream, br_boolean finish)
{
    size_t uncoded;
    unsigned const char* uncodedStart;
    size_t encodedSize;
    int operation;
    switch (stream->continueAt) {
        case 0:
            goto cont0;
        case 1:
            goto cont1;
        case 2:
            goto cont2;
        case 3:
            goto cont3;
        case 4:
            goto cont4;
        case 5:
            goto cont5;
    }
    cont0:
    uncoded = stream->availIn;
    uncodedStart = stream->nextIn;
    if (!stream->encoder) {
        stream->encoder = BrotliEncoderCreateInstance(NULL, NULL, NULL);
        br_set_compression_level(stream, stream->level);
        stream->offset = 0;
        stream->blockSize = 0;
        XXH64_reset(stream->xxh64, 0);
        if (!stream->encoder)
            return BR_ERROR;
    }
    if (stream->blockSize + uncoded > stream->maxBlockSize)
        uncoded = stream->maxBlockSize - stream->blockSize;
    operation = (stream->blockSize + uncoded == stream->maxBlockSize || finish) ? BROTLI_OPERATION_FINISH : BROTLI_OPERATION_PROCESS;
    if (!BrotliEncoderCompressStream(stream->encoder, operation, &uncoded, &uncodedStart, &stream->availOut, &stream->nextOut, &encodedSize))
        return BR_ERROR;
    XXH64_update(stream->xxh64, stream->nextIn, uncodedStart - stream->nextIn);
    stream->offset += encodedSize;
    stream->totalSize += uncodedStart - stream->nextIn;
    stream->blockSize += uncodedStart - stream->nextIn;
    stream->availIn -= uncodedStart - stream->nextIn;
    stream->nextIn = uncodedStart;
    if (operation == BROTLI_OPERATION_PROCESS)
        return BR_OK;
    BrotliEncoderDestroyInstance(stream->encoder);
    stream->encoder = NULL;
    for (stream->tmp1 = stream->blockSize; stream->tmp1 > 0x7f; stream->tmp1 >>= 7) {
        stream->continueAt = 1;
        if (stream->availOut == 0)
            return BR_OK;
        cont1:
        *stream->nextOut++ = stream->tmp1 & 0x7f;
        stream->availOut--;
    }
    stream->continueAt = 2;
    if (stream->availOut == 0)
        return BR_OK;
    cont2:
    *stream->nextOut++ = stream->tmp1 | 0x80;
    stream->availOut--;
    stream->tmp2 = XXH64_digest(stream->xxh64);
    for (stream->tmp1 = 0; stream->tmp1 < 8; stream->tmp1++) {
        stream->continueAt = 3;
        if (stream->availOut == 0)
            return BR_OK;
        cont3:
        *stream->nextOut++ = stream->tmp2 & 0xff;
        stream->availOut--;
        stream->tmp2 >>= 8;
    }
    stream->continueAt = 4;
    if (stream->availOut == 0)
        return BR_OK;
    cont4:
    if (!finish || stream->availIn) {
        *stream->nextOut++ = 0113;
        stream->availOut--;
        stream->continueAt = 5;
        if (stream->availOut == 0)
            return BR_OK;
        cont5:
        *stream->nextOut++ = 0;
        stream->availOut--;
        stream->continueAt = 0;
        goto cont0;
    }
    *stream->nextOut++ = 0x27;
    stream->availOut--;
    return BR_END;
}
