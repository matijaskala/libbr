#ifndef LIBBR_H
#define LIBBR_H

typedef struct _br_stream br_stream;
typedef int br_boolean;
typedef enum {
    BR_ERROR = 0,
    BR_OK = 1,
    BR_END = 2
} br_result;

#ifdef __cplusplus
extern "C" {
#endif

br_stream* br_init();
void br_fini(br_stream* stream);
void br_set_out_buffer(br_stream* stream, char *data, unsigned int maxlen);
void br_set_in_buffer(br_stream* stream, const char *data, unsigned int size);
int br_in_buffer_available(const br_stream* stream);
int br_out_buffer_available(const br_stream* stream);
br_result br_read_header(br_stream* stream);
br_result br_write_header(br_stream* stream, const char* filename);
br_result br_uncompress(br_stream* stream);
br_result br_compress(br_stream* stream, br_boolean finish);
void br_set_compression_level(br_stream* stream, int level);

#ifdef __cplusplus
}
#endif

#endif
